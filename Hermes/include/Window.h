#pragma once

#include <Hermes.h>

namespace Hermes
{
	class Window : public wxFrame
	{
	public:
		Window();
	private:
		void OnExit(wxCommandEvent& event);
		void OnHello(wxCommandEvent& event);
	};

	enum Menu {
		ID_HELLO = 1
	};
}