#pragma once

#include <Hermes.h>

namespace Hermes {

	class Application : public wxApp
	{
	public:
		virtual bool OnInit();
	};

}