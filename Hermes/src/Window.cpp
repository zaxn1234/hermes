#include <Hermes.h>
#include <Window.h>

namespace Hermes
{
	Window::Window()
		: wxFrame(NULL, wxID_ANY, "Hello world!")
	{
		wxMenu* menuFile = new wxMenu;
		menuFile->Append(Menu::ID_HELLO,
			"Hello world");
		menuFile->AppendSeparator();
		menuFile->Append(wxID_EXIT);

		wxMenuBar* menuBar = new wxMenuBar;
		menuBar->Append(menuFile, "File");

		SetMenuBar(menuBar);

		CreateStatusBar();
		SetStatusText("Welcome to Hermes");

		Bind(wxEVT_MENU, &Window::OnHello, this, Menu::ID_HELLO);
		Bind(wxEVT_MENU, &Window::OnExit, this, wxID_EXIT);
	}

	void Window::OnExit(wxCommandEvent& event)
	{
		Close(true);
	}

	void Window::OnHello(wxCommandEvent& event)
	{
		wxMessageBox("Hello World pressed!",
			"Test message box", wxOK | wxICON_INFORMATION);
	}
}