#include <Application.h>
#include <Window.h>

namespace Hermes
{
	bool Application::OnInit()
	{
		Window* window = new Window();
		window->Show(true);
		return true;
	}
}