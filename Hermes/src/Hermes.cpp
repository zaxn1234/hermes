/*
* Hermes is a simple mail client.
* 
* Developed by Izaak Webster (zaxn1234)
* (c) 2021 
* 
*/

#include <Hermes.h>
#include <Application.h>

//namespace Hermes
//{
//	/*int main(int argc, char** argv[])
//	{
//		printf("Hello world!\n");
//
//		return 0;
//	}*/
//
//}
wxIMPLEMENT_APP(Hermes::Application);
wxIMPLEMENT_WXWIN_MAIN_CONSOLE;