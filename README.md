Hermes Mail Client
----------------

Hermes is a simple STMP e-mail client

## Prerequisites
In order to build Hermes you will need the following:
- Windows 10
- Visual Studio 19
- wxWidgets 3.1.5

## Build Process
This is how I built the project, but I'm sure there are many ways to achieve this. 

1. Install `vcpkg` and install `wxWidgets`
    - `vcpkg install wxWidgets wxWidgets:x64-windows`
    - `vcpkg integrate install` (this automatically links installed packages to Visual Studio)
2. Open solution and build for wanted environment

